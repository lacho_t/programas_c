/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Jueves 13 de agosto del 2020*/

/* Este programa determina el total de dinero dentro de un monedero en el cual se conoce con certeza el valor de cada elemento*/
#include <stdio.h>

int main(){
	/*Declaracion de un arreglo que contiene el valor de cada elemento que se tiene dentro del monedero*/
	int monedero[] = {1, 5, 10, 20, 50};
	
	/*Declaracion de variables*/
	int x, y, total = 0, suma = 0, valor = 0, suma2 = 0, valor2 = 0;
	
	printf("Monedero");
	
	/*Recopilacion de datos*/
	for(x =0; x <= 2; x++ ){
		printf("\ncuantas monedas de %d %s", monedero[x], "tiene? :");
		scanf("%d", &valor);
		
		suma = suma + (monedero[x] * valor);
	}

	for(y = 3; y <= 4; y++){
		printf("\ncuantos billetes de $%d %s", monedero[y], "tiene? :");
		scanf("%d", &valor2);

		suma2 = suma2 + (monedero[y] * valor2);
	}
	/*suma de los valores obtenidos para posteriormente imprimirlos en pantalla*/
	total = suma + suma2;
	printf("\nEL TOTAL DE SU MONEDERO ES DE :$%d %s", total, "\n\n" );
}
