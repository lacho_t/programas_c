/*Raúl_Hernández_López*/
/*freeeenergy1975@gmail.com*/
/*Viernes 09 de Octubre del 2020*/

/*Determina el sueldo semanal de N trabajadores considerando que se*/
/*les descuenta 5% de su sueldo si ganan entre 0 y 150 pesos.*/
/*Se les descuenta 7% si ganan más de 150 pero menos de 300*/
/*y 9% si ganan más de 300 pero menos de 450.*/
/*Los datos son horas trabajadas, sueldo por hora y nombre de cada trabajador*/

#include <stdio.h>
/*Inicio del metodo principal*/
int main(){
	/*Declaracion de variables*/
	float Horas_Trabajadas, Sueldo_Hora, Sueldo_Semanal, Sueldo_Final, Horas_Pago;
	int Dias_Trabajados, número_trabajadores, x;
	/*Definicion de la longitud de caracteres*/
	typedef char Tamaño_Nombre[30];
	
	printf("\nNúmero de trabajadores :");
	scanf ("%d", &número_trabajadores);
	/*Almacena el nombre de los trabajadores*/
        Tamaño_Nombre Nombres[número_trabajadores];

	for(x = 0; x <= número_trabajadores - 1; x++){
		printf("\nNombre : ");
		scanf("%s", &Nombres[x]);
		printf("\nIngresa el pago por hora de trabajo $");
		scanf("%f", &Sueldo_Hora);
		printf("\nHoras de trabajo por dia: ");
		scanf("%f", &Horas_Trabajadas);
		printf("\nNúmero de dias trabajados por semana :");
		scanf("%d", &Dias_Trabajados);
		Sueldo_Semanal = (Sueldo_Hora * Horas_Trabajadas) * Dias_Trabajados;
	
		if ((Sueldo_Semanal >= 0) & (Sueldo_Semanal <= 150)){
			Sueldo_Final = Sueldo_Semanal * 0.95;
			printf("\nEl sueldo de %s es de [$%f]", Nombres[x], Sueldo_Final);
		}
		else if((Sueldo_Semanal > 150) & (Sueldo_Semanal < 300)){
			Sueldo_Final = Sueldo_Semanal * 0.93;
			printf("\nEl sueldo de %s es de [$%f]", Nombres[x], Sueldo_Final);
		}
		else if((Sueldo_Semanal >= 300) & (Sueldo_Semanal < 450)){
			Sueldo_Final = Sueldo_Semanal * 0.91;
                        printf("\nEl sueldo de %s es de [$%f]", Nombres[x], Sueldo_Final);
		}

		
	}
}
