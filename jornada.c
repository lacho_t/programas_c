/*RAÚL_HERNÁNDEZ_LÓPEZ
 *freeenrgy1975@gmailcomo
16 de octubre del 2020*/

/*Escriba un algoritmo que, dado el número de horas trabajadas por un empleado y el sueldo por hora, calcule el sueldo total de ese
empleado. Tenga en cuenta que las horas extras se pagan el doble.*/

#include <stdio.h>

int main(){
	/*Declaracion de variables*/
	float Extra, Horas_Extra, Jornada_Normal, Jornada_Extra, Hora_Pago;
	int Hora_Entrada, Hora_Salida, Total_Horas;
	/*Recopila datos*/
	printf("Pago por Hora $");
	scanf("%f", &Hora_Pago);
	printf("\nHora de entrada (Formato de 24 hrs)");
	scanf("%d", &Hora_Entrada);
	printf("\nHora de salida (Formato de 24 hrs)");
	scanf("%d", &Hora_Salida);
	Total_Horas = Hora_Salida - Hora_Entrada;
	 /*Calcula el monto de una jornada normal*/
	if(Total_Horas < 9 ){
	   Jornada_Normal = Hora_Pago * Total_Horas;
	   printf("Monto a cobrar [$%f]", Jornada_Normal);
	}
	/*Calcula el monto de una joranada con horas extra*/
	else if(Total_Horas > 8){
	   Horas_Extra = Total_Horas - 8;
	   Extra = Hora_Pago * 2;
	   Jornada_Extra = (Horas_Extra * Extra) + ( 8 * Hora_Pago );
	   printf("Monto a cobrar [$%f]", Jornada_Extra);
	}	
}/*Fin metodo main*/


