/*RAÚL_HERNÁNDEZ_LÓPEZ
 * freeenrgy1975@gmail.com
 *17 de octubre del 2020*/

/*Calcular el nuevo salario de un empleado si se le descuenta el 20% de
 *  su salario actual.*/

#include <stdio.h>
/*Inicio metodo main*/
int main(){
   /*Declaracion de variables*/
   float Salario_Actual, Descuento, Salario_Descuento;
   printf("Salario actual :");
   /*Calcula el sueldo del empleado con el descuento del 20%*/
   scanf("%f", &Salario_Actual);
   Salario_Descuento = Salario_Actual * 0.80; 
   Descuento = Salario_Actual * 0.20;
   /*Impresion de resultados*/
   printf("\nEl descuento es igual a $%f\nMonto a cobrar [$%f]\n", Descuento, Salario_Descuento);
   
}
