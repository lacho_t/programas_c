/*Raul_Hernandez_Lopez
 * freeenregy1975@gmail.com
 * 16 de octubre del 2020*/

/*Calcular el nuevo salario de un empleado si obtuvo un
 *incremento del 8% sobre su salario actual y un
 descuento de 2.5% por servicios.*/

#include <stdio.h>
/*Inicio metodo main*/
int main(){
	/*Declaracion de variables*/
	float Salario_Actual, Descuento, Nuevo_Salario, Salario_Final;
	/*Recopila datos*/
	printf("Ingresa tu salario :");
	scanf("%f", &Salario_Actual);
	Nuevo_Salario = Salario_Actual + (Salario_Actual * 0.08);
	Descuento = Nuevo_Salario * 0.025;
	Salario_Final = Nuevo_Salario - Descuento;
	/*Imprime resultados*/
	printf("\nSe te otorgo un incremento del 8%");   
	printf("\nSin embargo de se te descontara el 2.5 por ciento por servicios");
	printf("\nTu sueldo total es de [$ %f ]\n", Salario_Final);

}/*fin metodo main*/
