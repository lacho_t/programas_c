/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Viernes 14 de agosto del 2020*/

/*Este programa determina el tiempo que tarda un persona en hacer un recorrido*/
#include <stdio.h>


int main(){
	/*Declaracion de variables*/
	float Velocidad, Distancia, Tiempo;

	printf("paseo por vicicleta");
	
	/*Recopilacion y Procesamiento de datos*/
	printf("\nCual es la distancia en km que recorriste? : ");
	scanf("%f", &Distancia);
	
	printf("\nCon que velocidad 'km/hr'? : ");
	scanf("%f", &Velocidad);
	
	Tiempo = (Distancia / Velocidad);
	/*Impresion de resultados*/
	printf("El tiempo que tardaste en recorrer %f", Tiempo);
}
