/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Jueves 03 de septiembre del 2020*/

/*este programa permite almacenar un numero N de ventas y separarla de acuerdo a una serie de
 *condiciones: ventas mayores a 10,000 pero menores de 20,000 Ventas iguales o menores
 de 10,000... cuanto es el monto de cada una y el monto global.*/

#include <stdio.h>
/*Inicio el metod main*/
int main(){
	int x, posicion, numero, mayor = 0, menor = 0;
	float sumaMen = 0, sumaMay = 0, MG = 0;
	printf("\nCuantas ventas realizaste? :");
	scanf("%d", &numero);
	/*almacenamiento del valor de cada venta en el arreglo*/
	float ventas[numero];
	for( x = 0; x < numero; x++){
		
		posicion = x + 1;
		printf("ingresa el valor de la venta No. %d %s", posicion, " :");
		scanf("%f", &ventas[x]);
		/*En esta variable se almacena la suma de todas las ventas*/
		MG = MG + ventas[x];
		
		/*antes de que el ciclo continue tiene que pasar por un filtro compuesto de dos 
 * 		condicionales*/
		if((ventas[x] >= 0) && (ventas[x] <= 10000)){	
				sumaMen = sumaMen + ventas[x];
				menor = menor + 1;
		}
		else if((ventas[x] >10000) && (ventas[x] <= 20000)){
				sumaMay = sumaMay + ventas[x];
				mayor = mayor +1;
		}
	}
	/*impresion de resultados*/	
	printf("Realizaste un total de %d ventas menores o iguales a Diez mil y la suma de todas ellas es de :%f\n",menor, sumaMen);	
	printf("Realizaste un total de %d ventas mayores a Diez mil pero menores a Veinte mil y la suma de estas es de :%f\n",mayor, sumaMay);
	printf("El monto global es de :%f\n", MG);
}/*Fin del metodo main*/
