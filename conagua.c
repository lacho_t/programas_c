/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Vernes 09 de Octubre del 2020*/

/*determinar el pago que debe realizar una persona por el 
total de metros cúbicos que consume de agua*/

#include <stdio.h>
/*inicio del metodo main*/
int main(){
	/*Declaracion de variables*/
	float pago, costo_agua, metro_consumido;
	/*Recopilaion de datos*/
	printf("\nIngresa el costo por metro cubico de agua :");
	scanf("%f", &costo_agua);
	printf("\nIngresa los metros cubicos de agua consumidos :");
	scanf("%f", &metro_consumido);
	/*define el monto a pagar*/
	pago = costo_agua * metro_consumido;
	/*Impresion de resultados*/
	printf("\nTu pago es de [$%f]\n", pago);	
}/*fin metodo main*/
