/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Viernes 16 de octubre del 2020*/

/*En un hospital existen 3 áreas: Urgencias, Pediatría y Traumatología. El presupuesto anual del
manera: Pediatría 42% y Traumatología 21%.*/

#include <stdio.h>
/*Inicio metodo main*/
int main(){
   /*Declaracion de variables*/
   float Presupuesto, Presupuesto_Pediatria, Presupuesto_Traumatologia, Presupuesto_Urgencias; 
   /*Recopila datos*/
   printf("Ingresa el presupuesto total:");
   scanf("%f", &Presupuesto);
   Presupuesto_Pediatria = Presupuesto * 0.42;
   Presupuesto_Traumatologia = Presupuesto * 0.12;
   Presupuesto_Urgencias = Presupuesto - (Presupuesto_Pediatria + Presupuesto_Traumatologia);
   /*Impresion de resultados*/
   printf("\nLa distribucion del presupuesto queda de la siguiente manera :");
   printf("\nPresupuesto pedriatia [$%f]", Presupuesto_Pediatria);
   printf("\nPresupuesto traumatologia [%f]", Presupuesto_Traumatologia);
   printf("\nPresupuesto Urgencias [%f]", Presupuesto_Urgencias);
}

